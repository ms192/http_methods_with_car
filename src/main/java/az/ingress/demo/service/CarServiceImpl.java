package az.ingress.demo.service;

import az.ingress.demo.config.Config;
import az.ingress.demo.model.Car;
import az.ingress.demo.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CarServiceImpl implements CarService{
    private final CarRepository carRepository;

   /* @Value("${car.speed}")
    private Integer speed;*/

    @Value("${car.list}")
    private final List<Integer> list = new ArrayList<>();
    @Value("${car.listFromConfig}")
    private final List<String> listOfColors = new ArrayList<>();
    private final Config config;


    public CarServiceImpl(CarRepository carRepository, Config config) {
        this.carRepository = carRepository;
        this.config = config;
    }

    @Override
    public Car get(Integer id) {
        log.info("car service get method");
//        log.info(config.getListFromConfig());
        System.out.println(config.getListFromConfig());
        Optional<Car> car = carRepository.findById(id);
        if (car.isEmpty()){
            throw new RuntimeException("Car not found");
        }
        return car.get();
    }


    @Override
    public List<Car> getAll() {
        log.info("car service getAll method");
        List<Car> cars = carRepository.findAll();
        if (cars.isEmpty()){
            throw new RuntimeException("Cars not found");
        }
        return cars;
    }

    @Override
    public Car create(Car car) {
        log.info("car service create method");
        //car.setSpeed(balance);
        car.setSpeed(list.get(1));
        car.setColor(listOfColors.get(2));
        Car carInDb = carRepository.save(car);
        return carInDb;
    }

    @Override
    public Car update(Integer id, Car car) {
        log.info("car service update method");
        Car entity = carRepository.findById(id).orElseThrow(() -> new RuntimeException());
        entity.setModel(car.getModel());
        entity.setYears(car.getYears());
        entity.setColor(car.getColor());
        entity.setPrice(car.getPrice());
        entity.setCondition(car.getCondition());
        return entity;
    }

    @Override
    public void delete(Integer id) {
        log.info("car service delete method");
        carRepository.deleteById(id);
    }

}
