package az.ingress.demo.service;

import az.ingress.demo.model.Car;

import java.util.List;

public interface CarService {
    Car get(Integer id);
    List<Car> getAll();
    Car create(Car car);
    Car update(Integer id, Car car);
    void delete(Integer id);
}
