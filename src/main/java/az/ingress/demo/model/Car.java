package az.ingress.demo.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    Boolean condition;
    Integer years;
    Integer price;
    String model;
    String color;
    String type;
    Integer speed;

    private static Car instance;

    public static Car getInstance(){
        if (instance == null){
            synchronized (Car.class){
                if (instance == null){
                    instance = new Car();
                }
            }
        }
        return instance;
    }
}
